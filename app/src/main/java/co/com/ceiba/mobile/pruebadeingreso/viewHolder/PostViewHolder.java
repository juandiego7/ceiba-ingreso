package co.com.ceiba.mobile.pruebadeingreso.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import co.com.ceiba.mobile.pruebadeingreso.R;

public class PostViewHolder extends RecyclerView.ViewHolder {

    private TextView tvTitle;
    private TextView tvbody;


    public PostViewHolder(View itemView){
        super(itemView);
        tvTitle = itemView.findViewById(R.id.title);
        tvbody = itemView.findViewById(R.id.body);

    }

    public TextView getTvTitle() {
        return tvTitle;
    }

    public void setTvTitle(TextView tvTitle) {
        this.tvTitle = tvTitle;
    }

    public TextView getTvbody() {
        return tvbody;
    }

    public void setTvbody(TextView tvbody) {
        this.tvbody = tvbody;
    }

}
