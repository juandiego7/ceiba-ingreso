package co.com.ceiba.mobile.pruebadeingreso.module;

import co.com.ceiba.mobile.pruebadeingreso.contracts.dao.IPostDao;
import co.com.ceiba.mobile.pruebadeingreso.contracts.dao.IUserDao;
import co.com.ceiba.mobile.pruebadeingreso.contracts.repository.IPostRepository;
import co.com.ceiba.mobile.pruebadeingreso.contracts.repository.IUserRepository;
import co.com.ceiba.mobile.pruebadeingreso.contracts.rest.IPostService;
import co.com.ceiba.mobile.pruebadeingreso.contracts.rest.IUserService;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.repository.PostRepository;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.repository.UserRepository;
import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    static IUserRepository provideUserRepository(IUserDao userDao, IUserService userService) {
        return new UserRepository(userDao, userService);
    }

    @Provides
    static IPostRepository providePostRepository(IPostDao postDao, IPostService postService) {
        return new PostRepository(postDao, postService);
    }
}
