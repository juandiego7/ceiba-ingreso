package co.com.ceiba.mobile.pruebadeingreso.contracts.adapter;

import co.com.ceiba.mobile.pruebadeingreso.model.User;

public interface IUserAdapterListener {
    void itemsCount(int count);
    void onClickbuttonPost(User user);
}
