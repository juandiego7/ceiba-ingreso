package co.com.ceiba.mobile.pruebadeingreso.module;

import co.com.ceiba.mobile.pruebadeingreso.view.MainActivity;
import co.com.ceiba.mobile.pruebadeingreso.view.PostActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuildersModule {

    @ContributesAndroidInjector(
            modules = {
                    UserViewModelModule.class
            })
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector(
            modules = {
                    PostViewModelModule.class
            })
    abstract PostActivity contributePostActivity();

}