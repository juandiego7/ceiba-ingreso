package co.com.ceiba.mobile.pruebadeingreso.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;
import java.util.List;
import javax.inject.Inject;
import co.com.ceiba.mobile.pruebadeingreso.contracts.mapper.IPostMapper;
import co.com.ceiba.mobile.pruebadeingreso.contracts.repository.IPostRepository;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.PostEntity;
import co.com.ceiba.mobile.pruebadeingreso.model.Post;
import co.com.ceiba.mobile.pruebadeingreso.util.NetworkBoundResource;

public class PostViewModel extends ViewModel {

    private IPostRepository postRepository;
    private IPostMapper postMapper;
    private MediatorLiveData<NetworkBoundResource<List<Post>>> postsLiveData = new MediatorLiveData<>();

    @Inject
    public PostViewModel(IPostRepository postRepository, IPostMapper postMapper) {
        this.postRepository = postRepository;
        this.postMapper = postMapper;
    }

    public void getPosts(int id) {
        final LiveData<NetworkBoundResource<List<PostEntity>>> source = this.postRepository.getAllLocalByIdUser(id);
        final int idUser = id;
        postsLiveData.addSource(source, new Observer<NetworkBoundResource<List<PostEntity>>>() {
            @Override
            public void onChanged(@Nullable NetworkBoundResource<List<PostEntity>> listNetworkBoundResource) {
                List<Post> posts = postMapper.mapToModel(listNetworkBoundResource.data);
                if(posts.size() > 0){
                    NetworkBoundResource networkBoundResource = new NetworkBoundResource(
                            listNetworkBoundResource.status,
                            posts,
                            listNetworkBoundResource.message);
                    postsLiveData.setValue(networkBoundResource);
                    postsLiveData.removeSource(source);
                }else {
                    postsLiveData.removeSource(source);
                    getPostsRemote(idUser);
                }
            }
        });

    }

    public void getPostsRemote(int id) {
        final LiveData<NetworkBoundResource<List<PostEntity>>> source = this.postRepository.getAllRemoteByIdUser(id);

        postsLiveData.addSource(source, new Observer<NetworkBoundResource<List<PostEntity>>>() {
            @Override
            public void onChanged(@Nullable NetworkBoundResource<List<PostEntity>> listNetworkBoundResource) {
                List<Post> posts = postMapper.mapToModel(listNetworkBoundResource.data);
                NetworkBoundResource networkBoundResource = new NetworkBoundResource(
                        listNetworkBoundResource.status,
                        posts,
                        listNetworkBoundResource.message);
                postsLiveData.setValue(networkBoundResource);
                postsLiveData.removeSource(source);
            }
        });
    }

    public LiveData<NetworkBoundResource<List<Post>>> observerPosts() {
        return postsLiveData;
    }

}