package co.com.ceiba.mobile.pruebadeingreso.module;

import android.arch.lifecycle.ViewModelProvider;
import co.com.ceiba.mobile.pruebadeingreso.viewmodel.ViewModelProviderFactory;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelFactoryModule {
    @Binds
    public abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelProviderFactory viewModelProviderFactory);
}
