package co.com.ceiba.mobile.pruebadeingreso.contracts.mapper;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.PostEntity;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.UserEntity;
import co.com.ceiba.mobile.pruebadeingreso.model.Post;
import co.com.ceiba.mobile.pruebadeingreso.model.User;

public interface IPostMapper {
    Post mapToModel(PostEntity postEntity);
    List<Post> mapToModel(List<PostEntity> postEntities);
    PostEntity mapToEntity(Post post);
    List<PostEntity>  mapToEntity(List<Post> posts);
}
