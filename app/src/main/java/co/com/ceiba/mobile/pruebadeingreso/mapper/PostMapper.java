package co.com.ceiba.mobile.pruebadeingreso.mapper;

import java.util.ArrayList;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.contracts.mapper.IPostMapper;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.PostEntity;
import co.com.ceiba.mobile.pruebadeingreso.model.Post;

public class PostMapper implements IPostMapper {

    @Override
    public Post mapToModel(PostEntity postEntity) {
        Post postMapped = new Post();
        postMapped.setId(postEntity.getId());
        postMapped.setUserId(postEntity.getUserId());
        postMapped.setTitle(postEntity.getTitle());
        postMapped.setBody(postEntity.getBody());
        return postMapped;
    }

    @Override
    public List<Post> mapToModel(List<PostEntity> postEntities) {
        List<Post> postsMapped = new ArrayList<>();
        for (int i = 0; i < postEntities.size(); i++) {
            postsMapped.add(mapToModel(postEntities.get(i)));
        }
        return postsMapped;
    }

    @Override
    public PostEntity mapToEntity(Post post) {
        PostEntity postEntityMapped = new PostEntity();
        postEntityMapped.setId(post.getId());
        postEntityMapped.setUserId(post.getUserId());
        postEntityMapped.setTitle(post.getTitle());
        postEntityMapped.setBody(post.getBody());
        return postEntityMapped;
    }

    @Override
    public List<PostEntity> mapToEntity(List<Post> posts) {
        List<PostEntity> postEntitiesMapped = new ArrayList<>();
        for (int i = 0; i < posts.size(); i++) {
            postEntitiesMapped.add(mapToEntity(posts.get(i)));
        }
        return postEntitiesMapped;
    }
}
