package co.com.ceiba.mobile.pruebadeingreso.contracts.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.support.annotation.WorkerThread;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.UserEntity;
import io.reactivex.Flowable;
import io.reactivex.Maybe;

@Dao
public interface IUserDao {
    @Query("SELECT * FROM user")
    Flowable<List<UserEntity>> getAllUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllUsers(List<UserEntity> userEntities);

}
