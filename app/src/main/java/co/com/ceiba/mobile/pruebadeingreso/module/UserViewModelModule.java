package co.com.ceiba.mobile.pruebadeingreso.module;

import android.arch.lifecycle.ViewModel;
import co.com.ceiba.mobile.pruebadeingreso.annotation.ViewModelKey;
import co.com.ceiba.mobile.pruebadeingreso.viewmodel.UserViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class UserViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel.class)
    public abstract ViewModel bindUserViewModel(UserViewModel userViewModel);

}
