package co.com.ceiba.mobile.pruebadeingreso.task;

import android.os.AsyncTask;
import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.contracts.dao.IUserDao;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.UserEntity;


public class UserDaoTask extends AsyncTask<List<UserEntity>, Void, Void> {

    private IUserDao mAsyncTaskDao;

    public UserDaoTask(IUserDao dao) {
        mAsyncTaskDao = dao;
    }

    @Override
    protected Void doInBackground(List<UserEntity>... lists) {
        mAsyncTaskDao.insertAllUsers(lists[0]);
        return null;
    }
}