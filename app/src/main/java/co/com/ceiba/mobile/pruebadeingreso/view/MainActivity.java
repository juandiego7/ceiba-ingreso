package co.com.ceiba.mobile.pruebadeingreso.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.util.List;
import javax.inject.Inject;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.adapter.UserAdapter;
import co.com.ceiba.mobile.pruebadeingreso.contracts.adapter.IUserAdapterListener;
import co.com.ceiba.mobile.pruebadeingreso.model.User;
import co.com.ceiba.mobile.pruebadeingreso.util.NetworkBoundResource;
import co.com.ceiba.mobile.pruebadeingreso.viewmodel.UserViewModel;
import co.com.ceiba.mobile.pruebadeingreso.viewmodel.ViewModelProviderFactory;
import dagger.android.AndroidInjection;

public class MainActivity extends AppCompatActivity implements IUserAdapterListener {

    @Inject
    ViewModelProviderFactory viewModelProviderFactory;
    private UserViewModel userViewModel;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private UserAdapter userAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private EditText editTextSearch;
    private RelativeLayout content;
    private View emptyView;
    private boolean isEmptyViewVisible;
    private RelativeLayout.LayoutParams paramsEmptyView;
    private LayoutInflater layoutInflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userViewModel = ViewModelProviders.of(this, viewModelProviderFactory).get(UserViewModel.class);
        editTextSearch = findViewById(R.id.editTextSearch);
        recyclerView = findViewById(R.id.recyclerViewSearchResults);
        content = findViewById(R.id.content);
        layoutInflater = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        emptyView = layoutInflater.inflate(R.layout.empty_view, null);
        isEmptyViewVisible = true;
        paramsEmptyView = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        paramsEmptyView.addRule(RelativeLayout.BELOW, editTextSearch.getId());
        content.addView(emptyView, paramsEmptyView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        userAdapter = new UserAdapter(this);
        recyclerView.setAdapter(userAdapter);
        progressBar = findViewById(R.id.indeterminateBar);
        subscribeObserver();
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userAdapter.getFilter().filter(s.toString().toLowerCase());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        userViewModel.getUsers();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void itemsCount(int count) {
        Log.d("itemsCount", count+"");
        if (count == 0){
            showEmptyView();
        }else {
            hideEmptyView();
        }
    }

    @Override
    public void onClickbuttonPost(User user) {
        Intent intent = new Intent(this, PostActivity.class);
        intent.putExtra("userId", user.getId()+"");
        intent.putExtra("name", user.getName());
        intent.putExtra("phone", user.getPhone());
        intent.putExtra("email", user.getEmail());
        startActivity(intent);
    }

    private void subscribeObserver() {
        userViewModel.observerUsers().observe(this, new Observer<NetworkBoundResource<List<User>>>() {
            @Override
            public void onChanged(@Nullable NetworkBoundResource<List<User>> listNetworkBoundResource) {
                if (listNetworkBoundResource != null) {
                    switch (listNetworkBoundResource.status) {
                        case LOADING:
                            showProgressBar(true);
                            break;
                        case SUCCESS:
                            showProgressBar(false);
                            emptyView.setVisibility(View.GONE);
                            isEmptyViewVisible = false;
                            userAdapter.setUsers(listNetworkBoundResource.data);;
                            break;
                        case ERROR:
                            showProgressBar(false);
                            Toast.makeText(MainActivity.this,
                                    listNetworkBoundResource.message,
                                    Toast.LENGTH_SHORT)
                                    .show();
                            break;
                    }
                }
            }
        });
    }

    private void showEmptyView(){
        if(!isEmptyViewVisible){
            this.emptyView.setVisibility(View.VISIBLE);
            this.recyclerView.setVisibility(View.GONE);
            isEmptyViewVisible = true;
        }
    }

    private void hideEmptyView(){
        if(isEmptyViewVisible){
            this.emptyView.setVisibility(View.GONE);
            this.recyclerView.setVisibility(View.VISIBLE);
            isEmptyViewVisible = false;
        }
    }

    private void showProgressBar(boolean visible) {
        if (visible) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

}