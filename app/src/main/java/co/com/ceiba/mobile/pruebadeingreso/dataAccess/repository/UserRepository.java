package co.com.ceiba.mobile.pruebadeingreso.dataAccess.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.LiveDataReactiveStreams;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import co.com.ceiba.mobile.pruebadeingreso.contracts.dao.IUserDao;
import co.com.ceiba.mobile.pruebadeingreso.contracts.repository.IUserRepository;
import co.com.ceiba.mobile.pruebadeingreso.contracts.rest.IUserService;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.UserEntity;
import co.com.ceiba.mobile.pruebadeingreso.task.UserDaoTask;
import co.com.ceiba.mobile.pruebadeingreso.util.NetworkBoundResource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class UserRepository implements IUserRepository {

    private IUserDao userDao;
    private IUserService userService;
    private MediatorLiveData<NetworkBoundResource<List<UserEntity>>> liveData;

    @Inject
    public UserRepository(IUserDao userDao, IUserService userService) {
        this.userDao = userDao;
        this.userService = userService;
        liveData = new MediatorLiveData<>();
        final LiveData<NetworkBoundResource<List<UserEntity>>> source = this.getLocal();
        liveData.addSource(source, new Observer<NetworkBoundResource<List<UserEntity>>>() {
            @Override
            public void onChanged(@Nullable NetworkBoundResource<List<UserEntity>> listNetworkBoundResource) {
                liveData.setValue(listNetworkBoundResource);
                liveData.removeSource(source);
            }
        });
    }

    @Override
    public LiveData<NetworkBoundResource<List<UserEntity>>> getAllLocal() {
        return liveData;
    }

    @Override
    public LiveData<NetworkBoundResource<List<UserEntity>>> getAllRemote() {
        return LiveDataReactiveStreams.fromPublisher(
                this.userService.getUsers()
                        .onErrorReturn(new Function<Throwable, List<UserEntity>>() {
                            @Override
                            public List<UserEntity> apply(Throwable throwable) throws Exception {
                                return null;
                            }
                        })
                        .map(new Function<List<UserEntity>, NetworkBoundResource<List<UserEntity>>>() {
                            @Override
                            public NetworkBoundResource<List<UserEntity>> apply(List<UserEntity> users) throws Exception {
                                if (users == null) {
                                    return NetworkBoundResource.error("Empty data", null);
                                }
                                insert(users);
                                return NetworkBoundResource.success(users);
                            }
                        })
                        .subscribeOn(Schedulers.io())
        );
    }

    @Override
    public void insert(List<UserEntity> userEntities) {
        new UserDaoTask(userDao).execute(userEntities);
    }

    public LiveData<NetworkBoundResource<List<UserEntity>>> getLocal() {
        return LiveDataReactiveStreams.fromPublisher(
                this.userDao.getAllUsers()
                        .onErrorReturn(new Function<Throwable, List<UserEntity>>() {
                            @Override
                            public List<UserEntity> apply(Throwable throwable) throws Exception {
                                return null;
                            }
                        })
                        .map(new Function<List<UserEntity>, NetworkBoundResource<List<UserEntity>>>() {
                            @Override
                            public NetworkBoundResource<List<UserEntity>> apply(List<UserEntity> users) throws Exception {
                                if (users == null) {
                                    return NetworkBoundResource.error("Empty data", null);
                                }
                                return NetworkBoundResource.success(users);
                            }
                        })
                        .subscribeOn(Schedulers.io())
        );
    }
}
