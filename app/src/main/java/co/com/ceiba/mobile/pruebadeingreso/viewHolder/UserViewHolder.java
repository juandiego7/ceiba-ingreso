package co.com.ceiba.mobile.pruebadeingreso.viewHolder;

import co.com.ceiba.mobile.pruebadeingreso.R;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public  class UserViewHolder extends RecyclerView.ViewHolder {

    private TextView tvName;
    private TextView tvPhone;
    private TextView tvEmail;
    private Button btnViewPost;
    public UserViewHolder(View itemView){
        super(itemView);
        tvName = itemView.findViewById(R.id.name);
        tvPhone = itemView.findViewById(R.id.phone);
        tvEmail = itemView.findViewById(R.id.email);
        btnViewPost = itemView.findViewById(R.id.btn_view_post);
    }

    public TextView getTvName() {
        return tvName;
    }

    public void setTvName(TextView tvName) {
        this.tvName = tvName;
    }

    public TextView getTvPhone() {
        return tvPhone;
    }

    public void setTvPhone(TextView tvPhone) {
        this.tvPhone = tvPhone;
    }

    public TextView getTvEmail() {
        return tvEmail;
    }

    public void setTvEmail(TextView tvEmail) {
        this.tvEmail = tvEmail;
    }

    public Button getBtnViewPost() {
        return btnViewPost;
    }

    public void setBtnViewPost(Button btnViewPost) {
        this.btnViewPost = btnViewPost;
    }
}
