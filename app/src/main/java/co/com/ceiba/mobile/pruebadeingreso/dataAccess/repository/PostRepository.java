package co.com.ceiba.mobile.pruebadeingreso.dataAccess.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.LiveDataReactiveStreams;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import co.com.ceiba.mobile.pruebadeingreso.contracts.dao.IPostDao;
import co.com.ceiba.mobile.pruebadeingreso.contracts.repository.IPostRepository;
import co.com.ceiba.mobile.pruebadeingreso.contracts.rest.IPostService;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.PostEntity;
import co.com.ceiba.mobile.pruebadeingreso.task.PostDaoTask;
import co.com.ceiba.mobile.pruebadeingreso.util.NetworkBoundResource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class PostRepository implements IPostRepository {

    private IPostDao postDao;
    private IPostService postService;
    private MediatorLiveData<NetworkBoundResource<List<PostEntity>>> liveData;

    @Inject
    public PostRepository(IPostDao postDao, IPostService postService) {
        this.postDao = postDao;
        this.postService = postService;
        liveData = new MediatorLiveData<>();
    }

    @Override
    public LiveData<NetworkBoundResource<List<PostEntity>>> getAllLocalByIdUser(int id) {
        final LiveData<NetworkBoundResource<List<PostEntity>>> source = this.getLocal(id);
        liveData.addSource(source, new Observer<NetworkBoundResource<List<PostEntity>>>() {
            @Override
            public void onChanged(@Nullable NetworkBoundResource<List<PostEntity>> listNetworkBoundResource) {
                liveData.setValue(listNetworkBoundResource);
                liveData.removeSource(source);
            }
        });
       return liveData;
    }

    @Override
    public LiveData<NetworkBoundResource<List<PostEntity>>> getAllRemoteByIdUser(int id) {
        return LiveDataReactiveStreams.fromPublisher(
                this.postService.getPostsByIdUser(id)
                        .onErrorReturn(new Function<Throwable, List<PostEntity>>() {
                            @Override
                            public List<PostEntity> apply(Throwable throwable) throws Exception {
                                return null;
                            }
                        })
                        .map(new Function<List<PostEntity>, NetworkBoundResource<List<PostEntity>>>() {
                            @Override
                            public NetworkBoundResource<List<PostEntity>> apply(List<PostEntity> posts) throws Exception {
                                if (posts == null) {
                                    return NetworkBoundResource.error("Empty data", null);
                                }
                                insert(posts);
                                return NetworkBoundResource.success(posts);
                            }
                        })
                        .subscribeOn(Schedulers.io())
        );
    }

    @Override
    public void insert(List<PostEntity> postEntities) {
        new PostDaoTask(postDao).execute(postEntities);
    }

    public LiveData<NetworkBoundResource<List<PostEntity>>> getLocal(int id) {
        return LiveDataReactiveStreams.fromPublisher(
                this.postDao.getAllPostByIdUser(id)
                        .onErrorReturn(new Function<Throwable, List<PostEntity>>() {
                            @Override
                            public List<PostEntity> apply(Throwable throwable) throws Exception {
                                return null;
                            }
                        })
                        .map(new Function<List<PostEntity>, NetworkBoundResource<List<PostEntity>>>() {
                            @Override
                            public NetworkBoundResource<List<PostEntity>> apply(List<PostEntity> post) throws Exception {
                                if (post == null) {
                                    return NetworkBoundResource.error("Empty data", null);
                                }
                                return NetworkBoundResource.success(post);
                            }
                        })
                        .subscribeOn(Schedulers.io())
        );
    }
}
