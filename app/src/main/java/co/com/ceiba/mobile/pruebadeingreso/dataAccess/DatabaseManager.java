package co.com.ceiba.mobile.pruebadeingreso.dataAccess;

import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import co.com.ceiba.mobile.pruebadeingreso.contracts.dao.IPostDao;
import co.com.ceiba.mobile.pruebadeingreso.contracts.dao.IUserDao;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.PostEntity;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.UserEntity;

@Database(entities = {UserEntity.class, PostEntity.class}, version = 1)
public abstract class DatabaseManager extends RoomDatabase {
    public abstract IUserDao userDao();
    public abstract IPostDao postDao();

    private static final String DATABASE_NAME = "users_posts";
    private static volatile DatabaseManager instance;

    public static DatabaseManager getDatabase(final Application context) {
        if (instance == null) {
            synchronized (DatabaseManager.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            DatabaseManager.class, DATABASE_NAME)
                            .build();
                }
            }
        }
        return instance;
    }
}

