package co.com.ceiba.mobile.pruebadeingreso.task;

import android.os.AsyncTask;
import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.contracts.dao.IPostDao;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.PostEntity;


public class PostDaoTask extends AsyncTask<List<PostEntity>, Void, Void> {

    private IPostDao mAsyncTaskDao;

    public PostDaoTask(IPostDao dao) {
        mAsyncTaskDao = dao;
    }

    @Override
    protected Void doInBackground(List<PostEntity>... lists) {
        mAsyncTaskDao.insertPosts(lists[0]);
        return null;
    }
}