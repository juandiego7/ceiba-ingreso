package co.com.ceiba.mobile.pruebadeingreso.mapper;

import java.util.ArrayList;
import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.contracts.mapper.IUserMapper;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.UserEntity;
import co.com.ceiba.mobile.pruebadeingreso.model.User;

public class UserMapper implements IUserMapper {

    @Override
    public User mapToModel(UserEntity userEntity) {
        User userMapped = new User();
        userMapped.setId(userEntity.getId());
        userMapped.setName(userEntity.getName());
        userMapped.setPhone(userEntity.getPhone());
        userMapped.setEmail(userEntity.getEmail());
        return userMapped;
    }

    @Override
    public List<User> mapToModel(List<UserEntity> userEntities) {
        List<User> usersMapped = new ArrayList<>();
        for (int i = 0; i < userEntities.size(); i++) {
            usersMapped.add(mapToModel(userEntities.get(i)));
        }
        return usersMapped;
    }

    @Override
    public UserEntity mapToEntity(User user) {
        UserEntity userEntityMapped = new UserEntity();
        userEntityMapped.setId(user.getId());
        userEntityMapped.setName(user.getName());
        userEntityMapped.setPhone(user.getPhone());
        userEntityMapped.setEmail(user.getEmail());
        return userEntityMapped;
    }

    @Override
    public List<UserEntity> mapToEntity(List<User> users) {
        List<UserEntity> userEntitiesMapped = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            userEntitiesMapped.add(mapToEntity(users.get(i)));
        }
        return userEntitiesMapped;
    }
}
