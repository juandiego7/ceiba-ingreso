package co.com.ceiba.mobile.pruebadeingreso.module;

import javax.inject.Singleton;
import co.com.ceiba.mobile.pruebadeingreso.contracts.dao.IPostDao;
import co.com.ceiba.mobile.pruebadeingreso.contracts.dao.IUserDao;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.DatabaseManager;
import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    @Singleton
    @Provides
    IUserDao providesUserDao(DatabaseManager databaseManager) {
        return databaseManager.userDao();
    }

    @Singleton
    @Provides
    IPostDao providesPostDao(DatabaseManager databaseManager) {
        return databaseManager.postDao();
    }

}
