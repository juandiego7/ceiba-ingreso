package co.com.ceiba.mobile.pruebadeingreso.module;

import android.arch.lifecycle.ViewModel;
import co.com.ceiba.mobile.pruebadeingreso.annotation.ViewModelKey;
import co.com.ceiba.mobile.pruebadeingreso.viewmodel.PostViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class PostViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(PostViewModel.class)
    public abstract ViewModel bindPostViewModel(PostViewModel postViewModel);

}