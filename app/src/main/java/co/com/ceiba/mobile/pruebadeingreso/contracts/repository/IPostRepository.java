package co.com.ceiba.mobile.pruebadeingreso.contracts.repository;

import android.arch.lifecycle.LiveData;
import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.PostEntity;
import co.com.ceiba.mobile.pruebadeingreso.util.NetworkBoundResource;

public interface IPostRepository {
    LiveData<NetworkBoundResource<List<PostEntity>>> getAllLocalByIdUser(int id);
    LiveData<NetworkBoundResource<List<PostEntity>>> getAllRemoteByIdUser(int id);
    void insert(List<PostEntity> postEntities);
}
