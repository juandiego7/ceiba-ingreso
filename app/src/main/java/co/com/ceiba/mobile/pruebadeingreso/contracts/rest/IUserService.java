package co.com.ceiba.mobile.pruebadeingreso.contracts.rest;

import android.arch.lifecycle.LiveData;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.UserEntity;
import co.com.ceiba.mobile.pruebadeingreso.model.User;
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints;
import io.reactivex.Flowable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IUserService {

    @GET(Endpoints.GET_USERS)
    Flowable<List<UserEntity>> getUsers();
}
