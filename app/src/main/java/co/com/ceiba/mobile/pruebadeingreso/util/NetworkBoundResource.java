package co.com.ceiba.mobile.pruebadeingreso.util;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class NetworkBoundResource<T> {
    @NonNull
    public final NetworkBoundResourceStatus status;

    @Nullable
    public final T data;

    @Nullable
    public final String message;

    public NetworkBoundResource(@NonNull NetworkBoundResourceStatus status, @Nullable T data, @Nullable String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public static <T> NetworkBoundResource<T> success (@Nullable T data) {
        return new NetworkBoundResource<>(NetworkBoundResourceStatus.SUCCESS, data, null);
    }


    public static <T> NetworkBoundResource<T> error(@NonNull String msg, @Nullable T data) {
        return new NetworkBoundResource<>(NetworkBoundResourceStatus.ERROR, data, msg);
    }

    public static <T> NetworkBoundResource<T> loading(@Nullable T data) {
        return new NetworkBoundResource<>(NetworkBoundResourceStatus.LOADING, data, null);
    }

    public enum NetworkBoundResourceStatus { SUCCESS, ERROR, LOADING, FAILURE}
}
