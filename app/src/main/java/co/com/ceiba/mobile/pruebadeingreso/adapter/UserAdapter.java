package co.com.ceiba.mobile.pruebadeingreso.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import java.util.ArrayList;
import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.contracts.adapter.IUserAdapterListener;
import co.com.ceiba.mobile.pruebadeingreso.model.User;
import co.com.ceiba.mobile.pruebadeingreso.viewHolder.UserViewHolder;

public class UserAdapter extends RecyclerView.Adapter<UserViewHolder> implements Filterable {

    private List<User> users;
    private List<User> usersFiltered;
    private IUserAdapterListener listener;

    public UserAdapter(IUserAdapterListener listener){
        users = new ArrayList<>();
        usersFiltered = new ArrayList<>();
        this.listener = listener;
    }

    public void setUsers(List<User> users) {
        this.users = users;
        setUsersFiltered(users);
        notifyDataSetChanged();
    }

    public void setUsersFiltered(List<User> usersFiltered) {
        this.usersFiltered = usersFiltered;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_item, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        final User currentUser = usersFiltered.get(position);
        holder.getTvName().setText(currentUser.getName());
        holder.getTvPhone().setText(currentUser.getPhone());
        holder.getTvEmail().setText(currentUser.getEmail());
        holder.getBtnViewPost().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickbuttonPost(currentUser);
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString().toLowerCase();
                if(charString.isEmpty()){
                    usersFiltered = users;
                } else {
                    List<User> filteredList = new ArrayList<>();
                    for(User user: users){
                        if(user.getName().toLowerCase().contains(charString)){
                            filteredList.add(user);
                        }
                    }
                    usersFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = usersFiltered;
                filterResults.count = usersFiltered.size();
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                usersFiltered = (ArrayList<User>)results.values;
                listener.itemsCount(usersFiltered.size());
                notifyDataSetChanged();
            }
        };
    }
}
