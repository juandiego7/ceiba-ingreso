package co.com.ceiba.mobile.pruebadeingreso.module;

import co.com.ceiba.mobile.pruebadeingreso.contracts.rest.IPostService;
import co.com.ceiba.mobile.pruebadeingreso.contracts.rest.IUserService;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ApiServiceModule {

    @Provides
    static IUserService provideUserService(Retrofit retrofit){
        return retrofit.create(IUserService.class);
    }

    @Provides
    static IPostService providesPostService(Retrofit retrofit){
        return retrofit.create(IPostService.class);
    }

}
