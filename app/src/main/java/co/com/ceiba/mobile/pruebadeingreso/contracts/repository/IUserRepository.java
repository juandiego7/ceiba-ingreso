package co.com.ceiba.mobile.pruebadeingreso.contracts.repository;

import android.arch.lifecycle.LiveData;
import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.UserEntity;
import co.com.ceiba.mobile.pruebadeingreso.util.NetworkBoundResource;

public interface IUserRepository {
    LiveData<NetworkBoundResource<List<UserEntity>>> getAllLocal();
    LiveData<NetworkBoundResource<List<UserEntity>>> getAllRemote();
    void insert(List<UserEntity> userEntities);
}
