package co.com.ceiba.mobile.pruebadeingreso.module;

import javax.inject.Singleton;
import co.com.ceiba.mobile.pruebadeingreso.contracts.mapper.IPostMapper;
import co.com.ceiba.mobile.pruebadeingreso.contracts.mapper.IUserMapper;
import co.com.ceiba.mobile.pruebadeingreso.mapper.PostMapper;
import co.com.ceiba.mobile.pruebadeingreso.mapper.UserMapper;
import dagger.Module;
import dagger.Provides;

@Module
public class MapperModule {

    @Singleton
    @Provides
    IUserMapper providesUserMapper() {
        return new UserMapper();
    }

    @Singleton
    @Provides
    IPostMapper providesPostMapper() {
        return new PostMapper();
    }
}
