package co.com.ceiba.mobile.pruebadeingreso.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import javax.inject.Inject;
import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.adapter.PostAdapter;
import co.com.ceiba.mobile.pruebadeingreso.model.Post;
import co.com.ceiba.mobile.pruebadeingreso.model.User;
import co.com.ceiba.mobile.pruebadeingreso.util.NetworkBoundResource;
import co.com.ceiba.mobile.pruebadeingreso.viewmodel.PostViewModel;
import co.com.ceiba.mobile.pruebadeingreso.viewmodel.ViewModelProviderFactory;
import dagger.android.AndroidInjection;

import static java.lang.Float.parseFloat;

public class PostActivity extends AppCompatActivity {

    @Inject
    ViewModelProviderFactory viewModelProviderFactory;
    private PostViewModel postViewModel;
    private User user;
    private TextView tvName;
    private TextView tvPhone;
    private TextView tvEmail;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private PostAdapter postAdapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        postViewModel = ViewModelProviders.of(this, viewModelProviderFactory).get(PostViewModel.class);
        tvName = findViewById(R.id.name);
        tvPhone = findViewById(R.id.phone);
        tvEmail = findViewById(R.id.email);
        recyclerView = findViewById(R.id.recyclerViewPostsResults);
        Intent intent = getIntent();
        user = new User();
        user.setId(parseFloat(intent.getStringExtra("userId")));
        user.setName(intent.getStringExtra("name"));
        user.setPhone(intent.getStringExtra("phone"));
        user.setEmail(intent.getStringExtra("email"));
        tvName.setText(user.getName());
        tvPhone.setText(user.getPhone());
        tvEmail.setText(user.getEmail());
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        postAdapter = new PostAdapter();
        recyclerView.setAdapter(postAdapter);
        progressBar = findViewById(R.id.indeterminateBar);
        subscribeObserver();
    }

    @Override
    protected void onStart() {
        super.onStart();
        postViewModel.getPosts((int)user.getId());
    }

    private void subscribeObserver() {
        postViewModel.observerPosts().observe(this, new Observer<NetworkBoundResource<List<Post>>>() {
            @Override
            public void onChanged(@Nullable NetworkBoundResource<List<Post>> listNetworkBoundResource) {
                if (listNetworkBoundResource != null) {
                    switch (listNetworkBoundResource.status) {
                        case LOADING:
                            showProgressBar(true);
                            break;
                        case SUCCESS:
                            showProgressBar(false);
                            postAdapter.setPosts(listNetworkBoundResource.data);;
                            break;
                        case ERROR:
                            showProgressBar(false);
                            Toast.makeText(PostActivity.this,
                                    listNetworkBoundResource.message,
                                    Toast.LENGTH_SHORT)
                                    .show();
                            break;
                    }
                }
            }
        });
    }

    private void showProgressBar(boolean visible) {
        if (visible) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}
