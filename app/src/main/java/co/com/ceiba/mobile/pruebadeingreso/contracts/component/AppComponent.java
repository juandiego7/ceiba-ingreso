package co.com.ceiba.mobile.pruebadeingreso.contracts.component;

import android.app.Application;
import co.com.ceiba.mobile.pruebadeingreso.base.BaseApplication;
import co.com.ceiba.mobile.pruebadeingreso.module.ActivityBuildersModule;
import co.com.ceiba.mobile.pruebadeingreso.module.ApiServiceModule;
import co.com.ceiba.mobile.pruebadeingreso.module.AppModule;
import co.com.ceiba.mobile.pruebadeingreso.module.MapperModule;
import co.com.ceiba.mobile.pruebadeingreso.module.RepositoryModule;
import co.com.ceiba.mobile.pruebadeingreso.module.RoomModule;
import co.com.ceiba.mobile.pruebadeingreso.module.ViewModelFactoryModule;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import javax.inject.Singleton;
import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                ActivityBuildersModule.class,
                AppModule.class,
                RoomModule.class,
                ApiServiceModule.class,
                RepositoryModule.class,
                MapperModule.class,
                ViewModelFactoryModule.class
        })
public interface AppComponent extends AndroidInjector<BaseApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

}
