package co.com.ceiba.mobile.pruebadeingreso.module;

import co.com.ceiba.mobile.pruebadeingreso.dataAccess.DatabaseManager;
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import android.app.Application;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {


    @Singleton
    @Provides
    static DatabaseManager providesRoomDatabase(Application application) {
        return DatabaseManager.getDatabase(application);
    }

    @Singleton
    @Provides
    static Retrofit provideRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(Endpoints.URL_BASE)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
