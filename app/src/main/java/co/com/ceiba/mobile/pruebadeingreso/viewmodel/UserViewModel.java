package co.com.ceiba.mobile.pruebadeingreso.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;
import java.util.List;
import javax.inject.Inject;
import co.com.ceiba.mobile.pruebadeingreso.contracts.mapper.IUserMapper;
import co.com.ceiba.mobile.pruebadeingreso.contracts.repository.IUserRepository;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.UserEntity;
import co.com.ceiba.mobile.pruebadeingreso.model.User;
import co.com.ceiba.mobile.pruebadeingreso.util.NetworkBoundResource;

public class UserViewModel extends ViewModel {

    private IUserRepository userRepository;
    private IUserMapper userMapper;
    private MediatorLiveData<NetworkBoundResource<List<User>>> usersLiveData = new MediatorLiveData<>();

    @Inject
    public UserViewModel(IUserRepository userRepository, IUserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public void getUsers() {
        final LiveData<NetworkBoundResource<List<UserEntity>>> source = this.userRepository.getAllLocal();

        usersLiveData.addSource(source, new Observer<NetworkBoundResource<List<UserEntity>>>() {
            @Override
            public void onChanged(@Nullable NetworkBoundResource<List<UserEntity>> listNetworkBoundResource) {
                List<User> users = userMapper.mapToModel(listNetworkBoundResource.data);
                if(users.size() > 0){
                    NetworkBoundResource networkBoundResource = new NetworkBoundResource(
                            listNetworkBoundResource.status,
                            users,
                            listNetworkBoundResource.message);
                    usersLiveData.setValue(networkBoundResource);
                    usersLiveData.removeSource(source);
                }else {
                    usersLiveData.removeSource(source);
                    getUsersRemote();
                }
            }
        });

    }

    public void getUsersRemote() {
        final LiveData<NetworkBoundResource<List<UserEntity>>> source = this.userRepository.getAllRemote();

        usersLiveData.addSource(source, new Observer<NetworkBoundResource<List<UserEntity>>>() {
            @Override
            public void onChanged(@Nullable NetworkBoundResource<List<UserEntity>> listNetworkBoundResource) {
                List<User> users = userMapper.mapToModel(listNetworkBoundResource.data);
                NetworkBoundResource networkBoundResource = new NetworkBoundResource(
                        listNetworkBoundResource.status,
                        users,
                        listNetworkBoundResource.message);
                usersLiveData.setValue(networkBoundResource);
                usersLiveData.removeSource(source);
            }
        });
    }

    public LiveData<NetworkBoundResource<List<User>>> observerUsers() {
        return usersLiveData;
    }

}
