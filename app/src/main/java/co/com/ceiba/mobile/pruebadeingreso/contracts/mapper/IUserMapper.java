package co.com.ceiba.mobile.pruebadeingreso.contracts.mapper;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.UserEntity;
import co.com.ceiba.mobile.pruebadeingreso.model.User;

public interface IUserMapper {

    User mapToModel(UserEntity userEntity);
    List<User>  mapToModel(List<UserEntity> userEntities);
    UserEntity mapToEntity(User user);
    List<UserEntity>  mapToEntity(List<User> users);

}
