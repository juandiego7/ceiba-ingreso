package co.com.ceiba.mobile.pruebadeingreso.contracts.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.PostEntity;
import io.reactivex.Flowable;

@Dao
public interface IPostDao {
    @Query("SELECT * FROM post WHERE userId = :id")
    Flowable<List<PostEntity>> getAllPostByIdUser(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPosts(List<PostEntity> postEntities);
}
