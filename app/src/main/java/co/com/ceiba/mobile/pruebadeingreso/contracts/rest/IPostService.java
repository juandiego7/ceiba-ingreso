package co.com.ceiba.mobile.pruebadeingreso.contracts.rest;

import java.util.List;
import co.com.ceiba.mobile.pruebadeingreso.dataAccess.entity.PostEntity;
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints;
import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IPostService {

    @GET(Endpoints.GET_POST_USER)
    Flowable<List<PostEntity>> getPostsByIdUser(@Query("userid") int id);
}
